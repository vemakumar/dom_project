var body = document.getElementsByTagName('body')
var h1 = document.createElement('h1')
h1.textContent = 'Transfer List'
h1.setAttribute('id', 'header')
body[0].appendChild(h1)
var container1 = document.createElement('div')
container1.setAttribute('class', 'container')
body[0].appendChild(container1)

left = ['React', 'JS', 'Vue', ' Svelte']
buttons = ['>>', '>', '<', '<<']
right = ['React', 'JS', 'Vue']

function sec (container, leftside, text, i) {
  var sect = document.createElement('section')
  sect.classList.add('box')
  var input = document.createElement('input')
  var label = document.createElement('label')
  input.type = 'checkbox'
  input.id = `checkbox${i}`
  label.setAttribute('for', `checkbox${i}`)
  label.textContent = text
  sect.appendChild(input)
  sect.appendChild(label)
  leftside.appendChild(sect)
  container.appendChild(leftside)
}

function leftRight (l, pos) {
  var container = document.querySelector('.container')
  var leftside = document.createElement('div')
  leftside.setAttribute('class', pos)
  l.forEach((element, index) => {
    sec(container, leftside, element, index)
  })
}

function mid () {
  var container = document.querySelector('.container')
  var middle = document.createElement('div')
  middle.setAttribute('class', 'middle')
  buttons.forEach((element, index) => {
    var addbtn = document.createElement('button')
    addbtn.textContent = element
    addbtn.setAttribute('class', 'button')
    addbtn.classList.add(`button${index}`)
    middle.appendChild(addbtn)
  })
  container.appendChild(middle)
}

leftRight(left, 'left')
mid()
leftRight(right, 'right')

// var btn = document.querySelector('.button2')
// // console.log(btn);
// btn.addEventListener('click', e => {
//   var checkboxes = document.querySelectorAll('input[type="checkbox"]:checked')

//   var target = document.querySelectorAll('.left')[0]
//   // // console.log(target[0]);
//   checkboxes.forEach(ele => {
//     var label = ele.parentElement
//     target.appendChild(label)
//     ele.checked = false
//   })
// })

// var btn = document.querySelector('.button0')
// // console.log(btn);
// btn.addEventListener('click', e => {
//   var checkboxes = document.querySelectorAll('input[type="checkbox"]')
//   var target = document.querySelectorAll('.right')[0]
//   // // console.log(target[0]);
//   checkboxes.forEach(ele => {
//     var label = ele.parentElement

//     target.appendChild(label)
//     ele.checked = false
//   })
// })

// var btn = document.querySelector('.button3')
// // console.log(btn);
// btn.addEventListener('click', e => {
//   var checkboxes = document.querySelectorAll('input[type="checkbox"]')
//   var target = document.querySelectorAll('.left')[0]
//   // // console.log(target[0]);
//   checkboxes.forEach(ele => {
//     var label = ele.parentElement
//     target.appendChild(label)
//     ele.checked = false
//   })
// })
// var btn = document.querySelector('.button1')
// // console.log(btn);
// btn.addEventListener('click', e => {
//   var checkboxes = document.querySelectorAll('input[type="checkbox"]:checked')
//   var target = document.querySelectorAll('.right')[0]
//   // // console.log(target[0]);
//   checkboxes.forEach(ele => {
//     var label = ele.parentElement

//     target.appendChild(label)
//     ele.checked = false
//   })
// })

var btnpart = document.querySelector('.middle')

btnpart.addEventListener('click', e => {
  var c = e.target.classList[1]

  if (parseInt(c.slice(-1)) === 0 || parseInt(c.slice(-1)) === 1) {
    if (parseInt(c.slice(-1)) === 0) {
      var checkboxes = document.querySelectorAll('input[type="checkbox"]')
    } else {
      var checkboxes = document.querySelectorAll(
        'input[type="checkbox"]:checked'
      )
    }
    var target = document.querySelector('.right')
    checkboxes.forEach(ele => {
      var l = ele.parentElement
      console.log(l)
      target.appendChild(l)
      ele.checked=false
    })
  } else {
    if (parseInt(c.slice(-1)) === 3) {
      var checkboxes = document.querySelectorAll('input[type="checkbox"]')
    }
    else{
      var checkboxes = document.querySelectorAll('input[type="checkbox"]:checked')
    }
    var target = document.querySelector('.left')
    checkboxes.forEach(ele => {
      var l = ele.parentElement
      console.log(l)
      target.appendChild(l)
      ele.checked=false
    })
  }
})
